var cinemaAssignment = {
    cinema1: [
        {
            movieID: 1,
            date: [
                    "Oct 11, 2019",
                    "Oct 15, 2019"
                ]
        }
    ],
    cinema2: [
        {
            movieID: 1,
            date: [
                    "Oct 12, 2019"
                ]
        },
        {
            movieID: 2,
            date: [
                    "Oct 13, 2019",
                    "Oct 18, 2019"
                ]
        }
    ],
    cinema3: [
        {
            movieID: 1,
            date: [
                    "Oct 17, 2019"
                ]
        },
        {
            movieID: 3,
            date: [
                    "Oct 18, 2019",
                    "Oct 19, 2019"
                ]
        }
    ],
    cinema4: [
        {
            movieID: 1,
            date: [
                    "Oct 2, 2019"
                ]
        },
        {
            movieID: 3,
            date: [
                    "Oct 5, 2019",
                    "Oct 10, 2019"
                ]
        }
    ],

}