var movies = [
    {
        id: 1,
        title: "Movie 1",
        genre: "Drama",
        synopsis: "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Dolores labore commodi laudantium! Corrupti deserunt iste, consequatur vel laudantium totam fugit.",
        showDate: "10/11/2019",
        duration: "1hr 50mins",
        rating: 4,
        ticketPrice: 500,
        contentRating: "g",
        imgUrl: "assets/images/movies/joker.jpg"
    },

    {
        id: 2,
        title: "Movie 2",
        genre: "Drama",
        synopsis: "Lasfafasfasfafst amet consectetur, adipisicing elit. Dolores labore commodi laudantium! Corrupti deserunt iste, consequatur vel laudantium totam fugit.",
        showDate: "10/12/2019",
        duration: "2hr 50mins",
        rating: 3,
        ticketPrice: 200,
        contentRating: "pg",
        imgUrl: "assets/images/movies/Edward.jpg"
    },

    {
        id: 3,
        title: "Movie 3",
        genre: "Drama",
        synopsis: "dfgdhjghj hgjgh fgh fgh fafst amet consectetur, adipisicing elit. Dolores labore commodi laudantium! Corrupti deserunt iste, consequatur vel laudantium totam fugit.",
        showDate: "Oct 16, 2019",
        duration: "2hr 3mins",
        rating: 4,
        ticketPrice: 200,
        contentRating: "pg",
        imgUrl: "assets/images/movies/it_2.jpg"
    }

]

