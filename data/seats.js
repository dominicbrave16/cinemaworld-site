var cinemaSeats = {
    cinema1: { // [R][C]
        seats: [

            //I1        I2          I3       I4         I5       I6         I7           I8
            "taken", "available", "available", "available", "available", "available", "available", "taken",
            "taken", "available", "available", "available", "available", "available", "available", "available",
            "taken", "available", "available", "available", "available", "available", "available", "available",
            "taken", "available", "available", "taken", "available", "available", "available", "available",
            "available", "available", "available", "available", "available", "available", "available", "available",
            "taken", "available", "available", "available", "available", "available", "available", "available",
            "taken", "available", "available", "available", "available", "available", "available", "available",
            "taken", "available", "available", "available", "available", "available", "available", "available",
            "taken", "available", "available", "available", "available", "available", "available", "available"
        ]
    },
    cinema2: { // [R][C]
        seats: [
        // C   0     1     2     3     4     5     6     7          R    
            ["taken", "available", "available", "available", "available", "available", "available", "available"],  //  0   
            ["available", "available", "available", "available", "available", "available", "available", "available"],   //  1 
            ["available", "available", "available", "available", "available", "available", "available", "available"],   //  2
            ["available", "available", "available", "available", "available", "available", "available", "available"],   //  3
            ["taken", "available", "available", "taken", "available", "available", "available", "available"],   //  4
            ["available", "available", "available", "available", "available", "available", "available", "available"],   //  5
            ["available", "available", "available", "available", "available", "available", "available", "available"],   //  6
            ["available", "available", "available", "available", "available", "available", "available", "available"],   //  7
            ["available", "available", "available", "available", "available", "available", "available", "available"]    //  8
        ]
    },
    cinema3: { // [R][C]
        seats: [
        // C   0     1     2     3     4     5     6     7          R    
            ["taken", "available", "available", "available", "available", "available", "available", "available"],  //  0   
            ["available", "available", "available", "available", "available", "available", "available", "available"],   //  1 
            ["available", "available", "available", "available", "available", "available", "available", "available"],   //  2
            ["available", "available", "available", "available", "available", "available", "available", "available"],   //  3
            ["taken", "available", "available", "taken", "available", "available", "available", "available"],   //  4
            ["available", "available", "available", "available", "available", "available", "available", "available"],   //  5
            ["available", "available", "available", "available", "available", "available", "available", "available"],   //  6
            ["available", "available", "available", "available", "available", "available", "available", "available"],   //  7
            ["available", "available", "available", "available", "available", "available", "available", "available"]    //  8
        ]
    },
    cinema4: { // [R][C]
        seats: [
        // C   0     1     2     3     4     5     6     7          R    
            ["taken", "available", "available", "available", "available", "available", "available", "available"],  //  0   
            ["available", "available", "available", "available", "available", "available", "available", "available"],   //  1 
            ["available", "available", "available", "available", "available", "available", "available", "available"],   //  2
            ["available", "available", "available", "available", "available", "available", "available", "available"],   //  3
            ["taken", "available", "available", "taken", "available", "available", "available", "available"],   //  4
            ["available", "available", "available", "available", "available", "available", "available", "available"],   //  5
            ["available", "available", "available", "available", "available", "available", "available", "available"],   //  6
            ["available", "available", "available", "available", "available", "available", "available", "available"],   //  7
            ["available", "available", "available", "available", "available", "available", "available", "available"]    //  8
        ]
    }
}